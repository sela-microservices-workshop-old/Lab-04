# Microservices Workshop
Lab 04: Create a CD pipeline with Jenkins Blue Ocean

---

## Instructions

&nbsp;
 
### Deployment Job

 - Use blueocean to create a new pipeline using "Git" to get sources (set "microservices-calculator-app" as repository url)

&nbsp;

#### General Configuration

 - Add a pipeline environment variable "DOCKERHUB_USERNAME" with your dockerhub username
 

&nbsp;

#### First Stage (Test)

 - Configure the first stage (Test Env) to run in the "Test" node and set the environment variable "SERVER_IP":

![Image 2](Images/lab04-deployment-02.png)

 - Configure the first stage (Test Env) with two steps:
 
```
echo HOST_IP=${SERVER_IP} > .env
```
```
docker-compose pull
docker-compose up --force-recreate -d
```

![Image 1](Images/lab04-deployment-01.png)

&nbsp;

#### Second Stage (Approval)

 - Configure the second stage (Approval) using "Wait for interactive input":
 
```
Message: Deploy to Production?
Ok: Yes
```

&nbsp;

#### Third Stage (Production)

![Image 3](Images/lab04-deployment-03.png)

 - Configure the third step to run in the "Production" node and set the environment variable "SERVER_IP":

![Image 5](Images/lab04-deployment-05.png)

- Configure the third stage (Production Env) with two steps:
 
```
echo HOST_IP=${SERVER_IP} > .env
```
```
docker-compose pull
docker-compose up --force-recreate -d
```

![Image 4](Images/lab04-deployment-04.png)

 - Save the pipeline to create the jenkins file